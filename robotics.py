"""this file is a mosquito whacker activated by an ultrasonic eye sensor.
call init() first, then operate().

"""
import nxt.locator
from nxt.sensor import *
from playsound import playsound
from nxt.motor import *
import time


too_close=24
b =0
whacker=0
eye=0
def initialise():
    b= nxt.locator.find_one_brick()
    whacker= Motor(b, PORT_A)
    eye=Ultrasonic(b, PORT_1)

def operate(verbose=False,sentinel=[False]):
    b= nxt.locator.find_one_brick()
    whacker= Motor(b, PORT_A)

    #print type(b)

    while True:
        if verbose:
            print 'Ultrasonic:', Ultrasonic(b, PORT_1).get_sample()

        if Ultrasonic(b, PORT_1).get_sample()<too_close:
            whacker.turn(100, 90)
            whacker.turn(-100,90)
            playsound('/Users/willyspinner/Desktop/Python/leggo-hack/cropped_khaled.wav')
        if sentinel[0]==True:
            break

def operateTest(sentinel=[False]):
    while True:
        time.sleep(0.5)
        if sentinel[0]==True:
            break
