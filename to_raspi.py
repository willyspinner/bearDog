'''networking module for the raspi. This one connects to the raspi for hardware control of :

- Air conditioning
- lights

(robotics module is controlled by main computer.)

'''
#s is the socket.
s=0
import sys,socket,select
def sendMessage(message):
    #assumed that socket connection is already established
    s.send(message)

def operate(GLOBAL_SENTINEL,host,port):
#sys.path.insert(0,path_to_server)
#import client
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
    try :
        s.connect((host, port))
    except :
        print 'Unable to connect'
        sys.exit()

    while 1:
        if GLOBAL_SENTINEL[0]==True:
             break

        socket_list = [sys.stdin, s]

        # Get the list sockets which are readable
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])

        for sock in read_sockets:
            if sock == s:
                # incoming message from remote server, s
                data = sock.recv(4096)
                if not data :
                    print '\nDisconnected from chat server'
                    sys.exit()
                else :
                    #print data
                    #sys.stdout.write(data)
                    #sys.stdout.write('[Me] '); sys.stdout.flush()

            #else :
                # user entered a message
                #this should be disabled.
                #sys.stdin.readline()
                # BELOW IS STANDALONE.
                #msg = raw_input()

                #s.send(msg)
                #sys.stdout.write('[Me] '); sys.stdout.flush()

#    while True:        #print ("do something?")
    #    if GLOBAL_SENTINEL[0]==True:


    #        break
