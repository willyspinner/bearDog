import speech_recognition as sr

import pyaudio
import os
import random

# obtain audio from the microphone
name= "xander"
r = sr.Recognizer()
musdir="/Users/willyspinner/Desktop/bearbot_music"
lang="DUTCH"
# recognize speech using


def listen():
    with sr.Microphone() as source:
        print("Say something!")
        audio = r.listen(source)
    try:
        #--------------------------------ALL THE COMMANDS BELOW ------------------------
        said=r.recognize_sphinx(audio)
        print("Sphinx thinks you said: " + said)
        return said

    except sr.UnknownValueError:
        print("Sphinx could not understand audio")

    except sr.RequestError as e:
        print("Sphinx error; {0}".format(e))
