# -*- coding: utf-8 -*-
#from this strings class, we get a string output that is to be passed directly
# to os.system()

class strings:
    def __init__(this,lang,username):
        this.lang=lang
        this.username=username

    def changeLanguage(this,lang):
        this.lang=lang

    def listAvailableLanguages(this):
        print ["english","dutch","japanese"]

    def getPrefix(this):
        if this.lang=="english":
            return "say -v moira "
        if this.lang=="dutch":
            return "say -v xander "
        if this.lang=="japanese":
            return "say -v kyoko "



    #----------------------STRINGS ----------------------------

    def greet(hour,mins):
        #get suffix.
        suffix=""
        if lang=="english":
            suffix= " it is "+str(hour)+" O'clock "+str(mins)+". how can I help you?"
        elif lang=="dutch":
            suffix= " het is "+str(hour)+" uur "+str(mins) +".. hoe kan ik hulpvaardig zijn?"
        elif lang=="japanese":
            suffix= " 現在 "+str(hour)+" 時 "+str(mins)+"　分です. 今日は何をしますか。"

        #then take in time.
        if hour>4 and hour<12:
            if lang=="english":
                return getPrefix()+"good morning "+this.username+"."+suffix
            if lang=="dutch":
                return getPrefix()+"goeie morgen "+this.username+"."+suffix
            if lang=="japanese":
                return getPrefix()+" おはようございます、"+this.username+"."+suffix
        if hour>12 and hour<17:
            if lang=="english":
                return getPrefix()+"good afternoon, "+this.username+"."+suffix
            if lang=="dutch":
                return getPrefix()+"goeie middag, "+this.username+"."+suffix
            if lang=="japanese":
                return getPrefix()+" こんにちわ、"+this.username+"さん。"+suffix
        else:
            if lang=="english":
                return getPrefix()+"good evening, "+this.username+"."+suffix
            if lang=="dutch":
                return getPrefix()+"goeie avond, "+this.username+"."+suffix
            if lang=="japanese":
                return getPrefix()+" こんばんは、"+this.username+"さん。"+suffix
    class music:
        def playMusic():
            if lang=="english":
                return getPrefix()+"Playing music."
            if lang=="dutch":
                return getPrefix()+"Ik spele muziek."
            if lang=="japanese":
                return getPrefix()+" 音楽をします。"
    class robotics:
        def start():
            if lang=="english":
                return getPrefix()+" robotics module has successfuly initialised."
            if lang=="dutch":
                return getPrefix()+" NULL"
            if lang=="japanese":
                return getPrefix()+" NULL"
    def goodbye():
        if lang=="english":
            return getPrefix()+"goodbye!, "+this.username
        if lang=="dutch":
            return getPrefix()+"tot ziens, "+this.username
        if lang=="japanese":
            return getPrefix()+"さよなら、"+this,username+" さん。"
    def not_understood():
        if lang=="english":
            return getPrefix()+"I did not understand that."
        if lang=="dutch":
            return getPrefix()
        if lang=="japanese":
            return getPrefix()
    class template:
        # just a template for easier copy paste.
        if lang=="english":
            return getPrefix()
        if lang=="dutch":
            return getPrefix()
        if lang=="japanese":
            return getPrefix()
