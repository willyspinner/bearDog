import speech_recog as speech
import threading
import os
import sys
import time

from strings import strings
import robotics
import to_raspi
'''This file is to be run. Everything is controlled ,invoked, run from this master file.'''
#connet to raspi using socket below.

#path_to_server=sys.argv[1] #simply put $PWD for this in bash
host = sys.argv[1]
port = int(sys.argv[2])

#global variable that stops ALL Processes.
GLOBAL_SENTINEL=[False]#array so that it is mutable.
#TODO not sure if this global sentinel works such that when changed to true, the threads that have this var passed will stop. hm...
#(SOLVED: use some sort of array type )
#strings.
LANG="english"
USERNAME="Wilson"
strings= strings(LANG,USERNAME)

def parse(command):
    #command is a string delimited by whitespace.

    elif("on" in command.split() and ("lights" in command.split() or "light" in command.split())):
        to_raspi.sendMessage("light on")

    elif("off" in command.split() and ("lights" in command.split() or "light" in command.split())):
        to_raspi.sendMessage("light off")


    elif("music" in command.split() and "play" in command.split()):
        os.system(strings.music.playMusic())
        m=len(os.listdir(musdir))-1
        addr=os.listdir(musdir)[random.randint(0,m)]
        os.system("afplay "+musdir+"/"+addr)

    elif("goodbye" in command.split()):
            os.system(strings.goodbye())

    else:
        os.system(strings.not_understood())

def get_manual_user_input(sentinel=[False]):

    while sentinel[0]==False:
        print "enter something to beardog!"
        user_input= raw_input()

        if(user_input=="talk"):
            os.system("say wat zeg je?")
            parse(sprecog.listen())
        elif(user_input=="stop"):
            os.system("say bye bye")
            sentinel[0]=True
        else:
            parse(user_input)



#----------------------------------------------------------------------------------------------------------------
#                                               MAIN EXECUTION
#------------------------------------------------------------------------------------------------------------------

hour=time.localtime()[3]
mins=time.localtime()[4]
os.system(strings.greet(hour,mins))
#robotics.initialise()
try:
    #run the threads.
    user_input=threading.Thread(target = get_manual_user_input, args = [GLOBAL_SENTINEL]).start()
    #robotThread=threading.Thread(target = robotics.operate, args = [True,GLOBAL_SENTINEL]).start()
    #TODO NOT USING ROBOT.
    raspiThread=threading.Thread(target=to_raspi.operate,args=[GLOBAL_SENTINEL,host,port]).start()
except Exception,errtxt:
    print errtxt
